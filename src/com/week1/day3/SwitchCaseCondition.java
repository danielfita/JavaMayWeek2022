package com.week1.day3;

public class SwitchCaseCondition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		* int cMark=71;
		* 
		* switch(cMark) { case 100: System.out.println("s grade"); break; case 90:
		* System.out.println("a grade"); break; case 80: System.out.println("b grade");
		* break; case 70: System.out.println("c grade"); break; case 60:
		* System.out.println("d grade"); break; case 50: System.out.println("e grade");
		* break; case 40: System.out.println("u grade"); break; default:
		* System.out.println("incorrect mark"); }
		*/
		String browser=" IE                 ";
		switch(browser.toLowerCase().trim())
		{
		case "chrome": System.out.println("Launch Chrome"); break;
		case "ie": System.out.println("Launch IE"); break;
		case "firefox": System.out.println("Launch ff"); break;
		case "safari": System.out.println("Launch safari"); break;
		default: System.out.println("incorrect browser");

		}

	}

}

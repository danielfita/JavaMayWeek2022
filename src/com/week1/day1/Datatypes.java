package com.week1.day1;

public class Datatypes {

	public static void main(String[] args) {

		//datatypes
		// strict types
		//1. Primitive datatypes - No need to create an object
		//Boolean datatype - true/false
		//Numeric datatypes
		// Integral datatype - byte, short, int, long
		// Floating point datatypes - float, double
		//2. Non primitive datatypes - We need to create an object
		// String, Array, Class, Interface
		// Boolean 1 bit
		
		
		boolean javaClass = true;
		boolean seleniumClass = false;
		//Byte
		//size - 1 byte - 8 bit
		byte b = 20;
		b = 10;
		System.out.println(b);

	}

}

package com.week1.day2;

public class IncrementAndDecrement {

	public static void main(String[] args) {


		//a++=a+1
		//post increment
		byte a= 3;
		byte b=1;
		b=a++;
		System.out.println(a++); //4
		System.out.println(a); // 5
		System.out.println(b); //3
		//pre increment
		byte c= 3;
		byte d=1;
		d=++c;
		System.out.println(c); // 4
		System.out.println(d); // 4
		//post decrement
		byte e= 3;
		byte f=1;
		f=e--;
		System.out.println(e); //2
		System.out.println(f); //3
		//pre decrement
		byte g= 3;
		byte h=1;
		h=--g;
		System.out.println(g); // 2
		System.out.println(h); // 2

	}

}

package com.week3.day10;

public class TestCar {
	
	public static void main(String[] args) {
		
		Bmw tc = new Bmw();
		tc.start();
		tc.autoTransmission();
		tc.stop();
		Car.refuel();
		
		System.out.println("************************");
		
		
		Car c1 = new Car();
		c1.start();
		Car.refuel();
		c1.stop();
		
		System.out.println("************************");
		
		Car cc = new Bmw();
		
		cc.start();
		Car.refuel();
		cc.stop();
		
		System.out.println("************************");
		
		
//		Bmw b1 = (Bmw) new Car();
//		b1.start();
//		b1.autoTransmission();
//		b1.refuel();
//		b1.stop();
		 
		
		
	}

}

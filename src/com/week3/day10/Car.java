package com.week3.day10;

public class Car {
	
	public int start() {
		System.out.println("car -- start");
		return 0;
	}
	
	public void stop() {
		System.out.println("car -- stop");
	}
	
	public static  void refuel() {
		System.out.println("car -- refuel");
	}

}

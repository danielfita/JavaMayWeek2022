package com.week3.day10;

public class Bmw extends Car {
	
	public int start() {
		System.out.println("bmw -- start");
		return 0;
	}

	public void autoTransmission() {
		System.out.println("bmw -- autotransmission");
	}
}

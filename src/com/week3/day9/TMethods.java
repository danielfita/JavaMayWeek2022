package com.week3.day9;

public class TMethods {

	public static void main(String[] args) {
		TMethods TM = new TMethods();
		TM.t1();
	}

	public void t1() {
		System.out.println("This is method T1");
		t2();
	}

	public void t2() {
		System.out.println("This is method T2");
		t3();
	}

	public void t3() {
		System.out.println("This is method T3");
		t1();
	}

}

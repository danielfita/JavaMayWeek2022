package com.week3.day9;

public class EmployeeData {

	public static void main(String[] args) {


		EncapsulationConcept ec1 = new EncapsulationConcept();
		ec1.name = "Ravi";
		ec1.age = 21;
		ec1.setSalary(15000);
		
		System.out.println(ec1.name);
		System.out.println(ec1.age);
		
		System.out.println(ec1.getSalary());

		EncapsulationConcept ec2 = new EncapsulationConcept();
		ec2.name = "Ram";
		ec2.age = 22;
		ec2.setSalary(18000);
		
		System.out.println(ec2.name);
		System.out.println(ec2.age);
		
		System.out.println(ec2.getSalary());

	}

}

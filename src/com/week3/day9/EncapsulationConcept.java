package com.week3.day9;

public class EncapsulationConcept {
	
	// binding of object
	
	public String name;
	int age;
	private double salary;
	
	// getter and setter
	
	public void setSalary(double salary) {
		
		this.salary = salary;
		
	}
	
	public double getSalary() {
		return this.salary;
	}
	
	

}

package com.week3.day9;

public class ConstructorConcept {
	
	// It is a method where it's name equals the class name (i.e.,) [class name = method name]
	// it has no return type
	//it can be overloaded
	// It executes when object is created. 
	
	public ConstructorConcept()
	{
		System.out.println("default constructor");
	}
	
	public ConstructorConcept(int a)
	{
		System.out.println("single argument constructor");
	}

	public static void main(String[] args) {

		ConstructorConcept cc = new ConstructorConcept(10);
		
	}

}

package com.week3.day9;

public class Employee {
	
	String name;
	int age;
	String city;
	double salary;
	
	public Employee() {
		
	}
	public Employee(String name, double salary) {
		this.name = name;
		this.salary = salary;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e1 = new Employee("naushad", 100000);
		System.out.println(e1.name +" "+ e1.salary);
		
		Employee e2 = new Employee("suman", 10000000);
		System.out.println(e2.name +" "+ e2.salary);
		
		System.out.println(e1.name +" "+ e1.salary);
		
		Employee e3 = new Employee("naushad", 22, "chennai", 10000000);
		System.out.println(e3.name + e3.age + e3.city + e3.salary);
		
		Employee e4 = new Employee();
		System.out.println(e4.name + e4.age + e4.city + e4.salary);


	}

	public Employee(String name, int age, String city, double salary) {
		this.name = name;
		this.age = age;
		this.city = city;
		this.salary = salary;
	}

}

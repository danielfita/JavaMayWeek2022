package com.week3.day8;

public class MethodOverloading {
	
	// method names are same in same class
	// number/type of arguments differ

	public static void main(String[] args) {


		MethodOverloading mo = new MethodOverloading();
		mo.test(6, "Naushad");
	}

	private void test(String string) {

		System.out.println(string);		
	}

	public void test() {
		
		System.out.println("Imside test with no argument");

	}

	public void test(int a) {

		System.out.println("Imside test with one argument " + a);

	}
	
	public void test(int a, int b) {
		
		System.out.println("sum of a and b " + a+b);

	}
	
	public void test(String a, int b) {
		
		System.out.println("concatenate a and b " + a+b);

	}
	
	public void test(int a, String b) {
		
		System.out.println("Daniel concatenate a and b " + a+b);

	}

}

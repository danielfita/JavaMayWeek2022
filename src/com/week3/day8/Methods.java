package com.week3.day8;

import java.util.ArrayList;

public class Methods {

	public static void main(String[] args) {
		Methods m1 = new Methods();
		ArrayList cars = m1.getCars("seden");
		System.out.println(cars);
	}
	
	public ArrayList getCars(String carType) {
		ArrayList cars = new ArrayList();
		if(carType == "seden")
		{
			cars.add("city");
			cars.add("civic");
			cars.add("old baleno");
		}else if(carType == "suv")
		{
			cars.add("xuv 500");
			cars.add("scorpio");
		}else if(carType == "mini")
		{
			cars.add("cooper");
			cars.add("swift");
		}
		
		return cars;
	}

}

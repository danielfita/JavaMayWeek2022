package com.week3.day7;

public class User {

	String userName;
	int userAge;
	String userCity;
	
	public static void main(String[] args) {
		User u1 = new User();
		u1.userName = "Raja";
		u1.userAge = 20;
		u1.userCity = "Chennai";
		
		User u2 = new User();
		u2.userName = "Ashok";
		u2.userAge = 30;
		u2.userCity = "Goa";
		
		User u3 = new User();
		u3.userName = "Ram";
		u3.userAge = 25;
		u3.userCity = "Pune";
		
		System.out.println(u1.userName + ' ' + u1.userAge + ' ' + u1.userCity);
		System.out.println(u2.userName + ' ' + u2.userAge + ' ' + u2.userCity);
		System.out.println(u3.userName + ' ' + u3.userAge + ' ' + u3.userCity);
	}

}

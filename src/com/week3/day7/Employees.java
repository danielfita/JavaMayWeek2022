package com.week3.day7;

public class Employees {
	
	
	//class - blueprint/Template/category
	// objects - property of a class
	
	String name;
	int age;
	String id;
	char gender;
	boolean isActive;
	
	public static void main(String[] args) {
	
		Employees e1 = new Employees();
		e1.name = "Rocky";
		e1.age = 32;
		e1.id = "QA123";
		e1.gender = 'm';
		e1.isActive = true;
		
		System.out.println(e1.name+" "+e1.age+" "+e1.id+" "+e1.gender+" "+e1.isActive);
		
		Employees e2 = new Employees();
		System.out.println(e2.name+" "+e2.age+" "+e2.id+" "+e2.gender+" "+e2.isActive);
		
		new Employees().name="Ram";

	}

}

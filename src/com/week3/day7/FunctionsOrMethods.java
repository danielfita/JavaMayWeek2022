package com.week3.day7;

public class FunctionsOrMethods {
	
	// Methods
		// block that Performs a task
		// Method can't be created inside a method

	public static void main(String[] args) {

		FunctionsOrMethods f1 = new FunctionsOrMethods();
		f1.test();
		float x = (float)f1.addValues();
		
		System.out.println(x);
		
		f1.printString("Printing Passed value");
		
		f1.joinString("Vimal", " Raj");
		
	}
	
	// no argument
	// no return type
	public void test() {
		
		System.out.println("I'm inside test method");
	}
	
	// no argument
	// with return type	
	public int addValues() {
		int a,b,c;
		a=10;b=12;
		c= a+b;
		
		return c;
	}
	
	// with argument
		// no return type
	public void printString(String str) {
		
		System.out.println(str);
		joinString("Hello", "java");
		
	}
	
	// with argument
			// with return type
		public void joinString(String str, String str2) {
			
			System.out.println(str+str2);
			
		}

}
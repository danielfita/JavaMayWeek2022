package com.week4.day12;

public abstract class Page {
	
	public abstract void title();
	
	public abstract void URL();
	
	public void timeOut() {
		System.out.println("Page Page timeout after 5 sec");
	}
	
	public void pageLoad() {
		System.out.println("Page = Page Load");
	}
	
	public Page() {
		System.out.println("constructor");
	}
	
	public Page(String a) {
		System.out.println("string constructor" + a);
	}

}

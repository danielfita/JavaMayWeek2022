package com.week4.day12;

public class HomePage extends Page{

	@Override
	public void title() {

		System.out.println("HP -- title");
	}

	@Override
	public void URL() {
		System.out.println("HP -- URL");
	}
	
	public HomePage() {
		System.out.println("hp constructor");
	}
	
	public HomePage(int a) {
		super("fita");
		System.out.println("overload constructor" + a);
	}
	

}

package com.week4.day12;

public class TestPage {
	//object cannot be created for abstract class
	public static void main(String[] args) {
		HomePage h1 = new HomePage(30);
		h1.title();
		h1.URL();
		h1.timeOut();
		h1.pageLoad();
		
		System.out.println("***************************");
		
		LoginPage lp = new LoginPage();
		lp.title();
		lp.URL();
		lp.pageLoad();
		lp.timeOut();
		
		System.out.println("***************************");

		
		Page p1 = new LoginPage();
		p1.title(); // LP -- Title
		p1.pageLoad();// LP -- page load
		p1.timeOut();// Page Page timeout after 5 sec
		p1.URL();// LP -- URL
		
		
	}

}

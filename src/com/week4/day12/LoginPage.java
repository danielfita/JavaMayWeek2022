package com.week4.day12;

public class LoginPage extends Page{

	@Override
	public void title() {

		System.out.println("LP -- Title");
	}

	@Override
	public void URL() {

		System.out.println("LP -- URL");
	}
	
	public void pageLoad() {
		System.out.println("LP -- page load");
	}

}

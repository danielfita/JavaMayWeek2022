package com.week4.day11;

public class TestHospital {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//IndianMedical im = new IndianMedical(); object cannot be created for interface
		
		ForticHospital fh = new ForticHospital();
		fh.cardio();
		fh.ent();
		fh.onalogy();
		fh.pysio();
		fh.ortho();
		fh.neuro();
		
		//Top Casting
		IndianMedical im = new ForticHospital();
		im.neuro();
		im.onalogy();
		
		//Down Casting
		//ForticHospital FHH = new IndianMedical();
		
		
		fh.cash();
		IndianMedical.billing();
		System.out.println(UsMedical.minFees);
		fh.covid();
		fh.iCovid();
		
		
	}

}

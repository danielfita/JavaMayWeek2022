package com.week4.day11;

public interface IndianMedical extends IWho{
	public void onalogy();
	
	public void neuro();
	
	//After JDK 1.8 - Method body declaration is possible for static and default methods
	public static void billing() {
		System.out.println("Indian Medical Billing");
	}
	
	default void cash() {
		System.out.println("Indian Medical Cash");
	}
	
}

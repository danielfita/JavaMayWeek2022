package com.week4.day14;

public class DataConversion {

	public static void main(String[] args) {

		String a = "100";
		System.out.println(a+20);
		
		int qwer = Integer.parseInt(a);
		System.out.println(qwer+20);
		
		String b = "3.14";
		System.out.println(Float.parseFloat(b)+5);
		
		System.out.println(Byte.SIZE);
	}

}

package com.week4.day13;

public class ExceptionHandling {

	String a;

	public static void main(String[] args) {

		int a = 5;
		int b = 50;
		int c = 0;
		ExceptionHandling eh = new ExceptionHandling();
		// eh = null;
		eh.m1();
		int[] q = new int[2];
		try {
			c = b / a;
//			eh.a = "Ram";
			q[4] = 12;
			System.out.println("Daniel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(c);

	}

	public void m1() {
		m2();
	}

	public void m2() {
		m3();
	}

	public void m3() {
		try {
			System.out.println(5 / 0);
		} catch (Exception e) {
			try {
				System.out.println(5 / 0);
			} catch (Throwable e1) {
				e.printStackTrace();
			}
			e.printStackTrace();
		}
	}

}

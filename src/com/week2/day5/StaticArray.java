package com.week2.day5;

public class StaticArray {

	public static void main(String[] args) {
		
		// Limitations
				//1. Index size is not dynamic
				//2. unable to change the datatype

		int i[] = new int[4];

		i[0] = 10;
		i[1] = 25;
		i[2] = 30;
		i[3] = 40;

		System.out.println(i[0]+i[1]);
		System.out.println(i.length);
		
		for(int j=0;j<i.length;j++)
		{
			System.out.println(i[j]);
		}
		
		// casting
		char a ='A';
		System.out.println((int)a);
		
		//octal notation
		byte b = 066; // 6*8+6*1
		System.out.println(b);
		
		char[] c = new char[2];
		c[0]='p';
		c[1]='5';
		//c[2]='z'; // ArrayIndexOutOfBoundsException
		
		for(char eachC:c)
		{
			System.out.println(eachC);
		}
		
		String jl= "Java";
		
		Object[] empData = new Object[4];
		
				empData[0] = "Employee1";
				empData[1] = 198;
				empData[2] = 'M';
				empData[3] = true;
				
				for(Object eachEmp:empData)
				{
					System.out.println(eachEmp);
				}

				
	}

}

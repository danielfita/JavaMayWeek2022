package com.week2.day6;

import java.util.ArrayList;

public class DynamicArray {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//ClassName objectReference = new ClassName();
		
		ArrayList num = new ArrayList();
		
		num.add(100); //0
		num.add(200); //1
		num.add(300); //2
		
		System.out.println(num.size());
		
		num.add("Ravi"); //3
		num.add('M');//4
		num.add(12.33);//5
		num.add(true);//6
		
		System.out.println(num.size());
		System.out.println(num);
		System.out.println(num.get(3));
		
		//System.out.println(num.get(7)); // IndexOutOfBoundsException
		
		
		
		System.out.println("************************************");

		
		ArrayList data = new ArrayList(5);
		System.out.println(data.size());
		
		int [] a = new int[3];
		a[0] = 100;
		a[1] = 200;
		a[2] = 300;
		
		for(int j = 0;j < a.length; j++)
		{
			System.out.println(a[j]);
		}
		
		for(int i = 0; i < num.size();i++)
		{
			System.out.println(num.get(i));
		}

		for(Object eachnum:num)
		{
			System.out.println(eachnum);
		}
		
		System.out.println("************************************");
		
		
		
		ArrayList list = new ArrayList();
		list.add(12);//0
		list.add(28);

		list.add(39);
		list.add(45);

		System.out.println(list);
		list.add(3, 100);
		System.out.println(list);
		list.remove(3);
		System.out.println(list);



		
	}

}

package com.week5.day15;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class Set {

	public static void main(String[] args) {

		List b = new List();
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		linkedHashSet.addAll(b.testMethod());
		
		System.out.println(b.testMethod());
		System.out.println(linkedHashSet);
		
		TreeSet<String> treeSet = new TreeSet<String>();
		
		treeSet.addAll(b.testMethod());
		System.out.println(treeSet);
	}

}

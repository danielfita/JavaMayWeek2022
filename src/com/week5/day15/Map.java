package com.week5.day15;

import java.util.HashMap;

public class Map {

	public static void main(String[] args) {


		HashMap<Integer, String> hashMap = new HashMap<Integer,String>();
		hashMap.put(101, "Naushad");
		hashMap.put(102, "Suman");
		hashMap.put(103, "Daniel");
		
		hashMap.put(null, "Ravi");
		hashMap.put(null, "sita");
		hashMap.put(104, "Ram");

		
		System.out.println(hashMap);
		
		hashMap.remove(103);
		System.out.println(hashMap);
		
		System.out.println(hashMap.keySet());
		System.out.println(hashMap.values());
		
		System.out.println(hashMap.containsKey(109));
		
		String a = "Testing professional";
	
		char[] aArr = new char[a.length()];
		aArr = a.toCharArray();
		

		HashMap<Character, Integer> countChar = new HashMap<Character,Integer>();
		for(char aEach:aArr) {
			if(countChar.containsKey(aEach)) {
				countChar.put(aEach, countChar.get(aEach)+1);
			} else {
				countChar.put(aEach, 1);
			}
		}
		System.out.println(countChar);



	}

}
